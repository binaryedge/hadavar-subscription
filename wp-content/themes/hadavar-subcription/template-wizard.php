<?php
/**
 * Template Name: Wizard Template
 */
?>

<div class="container">
	<?php while (have_posts()) : the_post(); ?>
		<?php get_template_part('templates/content', 'wizard'); ?>
	<?php endwhile; ?>
</div>
