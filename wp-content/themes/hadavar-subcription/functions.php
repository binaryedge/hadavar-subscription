<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
	'lib/assets.php',    // Scripts and stylesheets
	'lib/extras.php',    // Custom functions
	'lib/membership.php',
	'lib/profile.php',
	'lib/setup.php',     // Theme setup
	'lib/titles.php',    // Page titles
	'lib/wrapper.php',   // Theme wrapper class
	'lib/customizer.php' // Theme customizer
];

foreach ($sage_includes as $file) {
	if (!$filepath = locate_template($file)) {
		trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
	}

	require_once $filepath;
}
unset($file, $filepath);


function style_scripts_frontend () {
	wp_dequeue_style('upme_css');
	wp_dequeue_style('upme_responsive');
}
add_action('upme_add_style_scripts_frontend', 'style_scripts_frontend');

function style_scripts_footer () {
	wp_dequeue_script('upme_custom');
}
add_action('wp_footer', 'style_scripts_footer');
