<?php
/**
 * Template Name: Simple Generic Template
 */
?>
<div class="container">
	<?php while (have_posts()) : the_post(); ?>
	
	<div class="row">
		<div class="col-sm-12">
			<div class="process-container px-4 py-5 mx-auto my-5">
				<h1><?php the_title(); ?></h1>

				<?php the_content(); ?>
			</div>
		</div>
	</div>
	
	<?php endwhile; ?>
</div>