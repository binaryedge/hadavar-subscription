<?php
/**
 * Template Name: Packages Template
 */
?>

<div class="container">
	<?php while (have_posts()) : the_post(); ?>
		<?php get_template_part('templates/content', 'packages'); ?>
	<?php endwhile; ?>
</div>
