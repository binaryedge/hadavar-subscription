<?php
/**
 * Template Name: List Template
 */
?>

<div class="container">
	<?php while (have_posts()) : the_post(); ?>
		<?php get_template_part('templates/content', 'list'); ?>
	<?php endwhile; ?>
</div>
