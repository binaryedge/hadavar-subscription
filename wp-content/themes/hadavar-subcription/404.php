<div class="container">
	<div class="row justify-content-center">
		<div class="col-8">
			<div class="card my-5">
			  <div class="card-block">
			  	<h1>404 Not Found</h1>
			    <h4>Your requested page was not found on our application.</h4>
			    
			    <a href="<?= esc_url(home_url('/')); ?>" class="btn btn-blue mt-4">Return to Home</a>
			  </div>
			</div>
		</div>
	</div>
</div>


