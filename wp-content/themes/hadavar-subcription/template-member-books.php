<?php
/**
 * Template Name: Member Books Template
 */
?>

<div class="container">
	<?php while (have_posts()) : the_post(); ?>
		<?php get_template_part('templates/content', 'member-books'); ?>
	<?php endwhile; ?>
</div>
