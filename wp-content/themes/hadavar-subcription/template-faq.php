<?php
/**
 * Template Name: FAQ Template
 */
?>

<div class="container">
<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
  <?php get_template_part('templates/content', 'faq'); ?>
<?php endwhile; ?>
</div>
