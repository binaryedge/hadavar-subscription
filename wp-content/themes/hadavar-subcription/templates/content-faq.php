<div class="row">
	
	<div class="faq">
		<div class="container">			
			<div id="accordion" class="faq-list" role="tablist" aria-multiselectable="true">
			<?php
				$faq_query = new WP_Query(array(
					'post_type' => 'faq',
					'posts_per_page' => -1,
					'order' => 'asc'
				));

				while ($faq_query->have_posts()): $faq_query->the_post();
			?>
				<div class="card">
					<div class="card-header" role="tab" id="faq-heading-<?php the_ID(); ?>">
						<h5 class="mb-0">
							<a data-toggle="collapse" data-parent="#accordion" href="#faq-collapse-<?php the_ID(); ?>" aria-expanded="true" aria-controls="#faq-collapse-<?php the_ID(); ?>" class="collapsed">
								<img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/accordion-arrow.png" class="accordion-arrow">
								<?php the_title(); ?>
							</a>
						</h5>
					</div>
					<div class="collapse" id="faq-collapse-<?php the_ID(); ?>" role="tabpanel" aria-labelledby="faq-heading-<?php the_ID(); ?>">
						<div class="card-block">
							<?php the_content(); ?>
						</div>
					</div>
				</div>
			<?php endwhile; wp_reset_postdata(); ?>
			</div>
		</div>
	</div>
	
</div>



