<?php

use Hadavar\Membership as HMember;


global $current_user;
get_currentuserinfo();

$user_credits = HMember\get_current_user_credits();

?>
	<div class="dash-card mb-5">
		<div class="row">
			<div class="col-sm-6">
				<div class="d-inline-flex welcome">
					<i class="fa fa-user-circle-o mr-3" aria-hidden="true"></i>	歡迎, <?php echo $current_user->user_firstname; ?>!
				</div>
			</div>
			<div class="col-sm-6">
				<div class="d-inline-flex align-items-center p-2 pr-4">你現在正訂閱 <b>「<?php echo HMember\get_current_user_subscription_name(); ?>」</b> 計劃:</div>
				<div class="d-inline-flex align-items-center p-2">
					<div class="align-self-center">
						<img src="<?php echo get_stylesheet_directory_uri() ?>/dist/	images/home-courses.png" class="mr-2" alt="Courses">
					</div>

					<div class="credit-count">
						<?php
							$course_permissions = get_field('course_permissions', "user_{$current_user->ID}");
							echo count($course_permissions);
						?> 聖經課程
					</div>
					
				</div>
				<div class="d-inline-flex align-items-center p-2">
					<div class="align-self-center">
						<img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/home-books.png" class="mr-2" alt="Books">
					</div>				
					<div class="credit-count">
						<?php
							$book_permissions = get_field('book_permissions', "user_{$current_user->ID}");
							echo count($book_permissions);
						?> 書籍
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php
			$book_has_access = HMember\get_books_with_permissions();

			$books_query = new WP_Query(array(
				'post_type' => 'books',
				'post__in' => $book_has_access,
				'posts_per_page' => -1,
			));
	?>

	<div class="row">
		<?php while ($books_query->have_posts()): $books_query->the_post(); ?>
		<div class="col-sm-4">
			<div class="card content-card mb-3">
				<div class="card-header" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);">
			    <span class="invisible">Featured</span>
			  </div>
			  <div class="card-block text-center">
			    <h4 class="card-title"><?php the_title(); ?></h4>
			    <h6 class="mt-0 mb-2">作者: <?php the_field('author'); ?></h6>
		      <?php if (get_field('translator')): ?>
		      <h6 class="mt-0 mb-3">翻譯員: <?php the_field('translator'); ?></h6>
		    	<?php endif; ?>
			    <p class="card-text"><?php the_excerpt(); ?></p>
			    <a href="<?php echo home_url('/books-list/') ?>#<?php echo get_the_ID(); ?>" class="btn btn-blue" target="_blank">詳細資料</a>
			  </div>
			</div>
		</div>
		<?php endwhile; wp_reset_query(); ?>
	</div>

