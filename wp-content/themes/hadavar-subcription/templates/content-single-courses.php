<div class="container"><?php

use Hadavar\Membership as HMember;
use Carbon\Carbon;


global $post;
global $current_user;

if ($post->post_parent != 0 ):
	// echo 'Course page';
	$course_progress = HMember\get_course_progress($post->post_parent);

	$prev_course = null; $next_course = null; $current_course = null;
	$course_childen = get_children(array(
		'post_parent' => $post->post_parent,
		'post_type' => 'courses',
		'order' => 'ASC',
		'orderby' => 'menu_order',
	));
	$course_IDs = array_keys($course_childen);
	foreach ($course_IDs as $cindex => $cid) {
		if ($cid == $post->ID) {
			$current_course = $cid;
			if (($cindex - 1) >= 0) $prev_course = $course_IDs[$cindex - 1];
			if (($cindex + 1) < count($course_IDs)) $next_course = $course_IDs[$cindex + 1];
		}
	}
?>

<?php while (have_posts()) : the_post(); ?>
<div class="single-child-course">
	<article <?php post_class(); ?>>
		<header>
			<div class="row d-flex align-items-center">
				<div class="col-sm-2 article-num">
					<?php echo $post->menu_order + 1; ?>
				</div>
				<div class="col-sm-10">
					<h3 class="mt-0"><?php echo get_the_title($post->post_parent); ?></h3>
					<h1 class="entry-title mb-0"><?php the_title(); ?></h1>
				</div>
			</div>
		</header>

		<div class="entry-content">
			<div class="row">
				<div class="col-sm-6">
					<div class="embed-container">
						<?php the_field('video'); ?>
					</div>
					
					<?php the_content(); ?>
				</div>
				<div class="col-sm-6">
					<h6><?php the_field('side_title_1'); ?></h6>
					<h4 class="mb-3"><?php the_field('side_title_2'); ?></h4>

					<div class="verses">
					<?php if (have_rows('verses')): while (have_rows('verses')): the_row(); ?>
						<div class="row">
							<div class="col-sm-1">
								<div class="chapter"><?php the_sub_field('chapter_and_verse'); ?></div>
							</div>
							<div class="col-sm-11">
								<?php the_sub_field('content'); ?>
							</div>
						</div>
					<?php endwhile; endif; ?>
					</div>
				</div>
			</div>
		</div>
		<?php if ($course_progress[$post->ID] == 2): ?>
			<div class="complete finished">
				<form action="<?php echo home_url('/') ?>" method="POST">
					<input type="hidden" name="action" value="undo_course">
					<input type="hidden" name="course_ID" value="<?php echo $post->ID; ?>">
					<input type="hidden" name="next_course_ID" value="<?php echo $next_course; ?>">

					<button type="submit" class="btn btn-block">
						<i class="fa" aria-hidden="true"></i>
						尚未完成此課
					</button>
				</form>
			</div>
		<?php else: ?>
			<div class="complete">
				<form action="<?php echo home_url('/') ?>" method="POST">
					<input type="hidden" name="action" value="complete_course">
					<input type="hidden" name="course_ID" value="<?php echo $post->ID; ?>">
					<input type="hidden" name="next_course_ID" value="<?php echo $next_course; ?>">

					<button type="submit" class="btn btn-block">
						<i class="fa" aria-hidden="true"></i>
						已經完成此課
					</button>
				</form>
			</div>
		<?php endif; ?>
		<footer>
			<a class="back-to-course btn-black" href="<?php echo get_permalink($post->post_parent); ?>">
				<i class="fa fa-angle-left" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;
				返回課程
			</a>


			<?php if ($course_progress[$post->ID] != 2 && $next_course != null): ?>
				<a class="next-course btn disabled">請完成此課再前往下一課</a>
			<?php elseif ($next_course == null): ?>
				<a class="next-course btn disabled">課程完結</a>
			<?php else: ?>
				<a class="next-course btn-blue" href="<?php echo $next_course != null ? get_permalink($next_course) : '#' ?>">
					下一課
					&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right" aria-hidden="true"></i>
				</a>
			<?php endif; ?>
			
			<?php if ($prev_course == null): ?>
				<a class="prev-course btn disabled">
					<i class="fa fa-angle-left" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;
					前一課
				</a>
			<?php else: ?>
				<a class="prev-course btn-black" href="<?php echo $prev_course != null ? get_permalink($prev_course) : '#' ?>">
					<i class="fa fa-angle-left" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;
					前一課
				</a>
			<?php endif; ?>
		</footer>
	</article>
</div>
<?php endwhile; ?>

<?php else: 
	// echo 'Base Parent Page';
?>

<?php while (have_posts()) : the_post(); ?>
<div class="single-base-course">
	<article <?php post_class(); ?>>
		<header class="py-4 px-5">
			<div class="row">
				<div class="col-sm-6 align-self-center">
					<h1 class="entry-title"><?php the_title(); ?></h1>
				</div>
				<div class="col-sm-6">
					<div class="teacher-info p-4">
						<?php while (have_rows('additional_content')) : the_row(); ?>
						<h3><?php the_sub_field('title'); ?></h3>
						<p class="mb-0"><?php the_sub_field('content'); ?></p>
						<?php endwhile; ?>
					</div>
				</div>
			</div>
		</header>
		
		<div class="entry-content p-5">
			<div class="row">
				<?php
					$sub_end = get_field('subscription_end', "user_{$current_user->ID}");
					
					if ($_GET['pv'] == '1') {
				?>
				<div class="col-12">
					<div class="alert alert-info">
						You don't have subscription towards this course. Please upgrade your subscription <a href="<?php echo home_url('/home/plan-modification') ?>">here</a>.
					</div>
				</div>
				<?php
					}
					elseif ($sub_end) {
						$carbon_now = Carbon::now('Asia/Hong_Kong');
						$carbon_exp = Carbon::createFromFormat('d/m/Y H:i', $sub_end . ' 23:59', 'Asia/Hong_Kong');
						if ($carbon_now->gte($carbon_exp)) :
				?>
				<div class="col-sm-12">
					<div class="alert alert-warning mb-5">
						Your Plan has expired on <?php the_field('subscription_end', "user_{$current_user->ID}"); ?>. Please update your subscription <a href="<?php echo home_url('/home/plan-modification') ?>">here</a>.
					</div>
				</div>
				<?php
						endif;
					}
				?>

				<div class="col-sm-6">
					<?php the_content(); ?>
				</div>
				<div class="col-sm-6">
					<?php
						$course_progress = HMember\get_course_progress($post->ID);

						$i = 0;
						$courses_args = array(
							'post_type'      => 'courses',
							'posts_per_page' => -1,
							'post_parent'    => $post->ID,
							'order'          => 'ASC',
							'orderby'        => 'menu_order'
						);

						$base_course_id = $post->ID;
						$courses_query = new WP_Query($courses_args);

						if ($courses_query->have_posts()):
							$course_not_started = true;
							foreach ($course_progress as $p) {
								if ($p != 0) {
									$course_not_started = false;
									break;
								}
							}
					?>
					<ul class="class-list pl-0 mb-0">
						<?php while ($courses_query->have_posts()): $courses_query->the_post(); 
							$i++;
						?>
						<li class="<?php
							$cp = $course_progress[$post->ID];
							if ($course_not_started && $i == 1) echo 'started';
							else if ($cp == 1) echo 'started';
							else if ($cp == 2) echo 'finished';
						?>">
							<a <?php
								if ($cp != null || ($course_not_started && $i == 1)) {
									echo 'href="' . get_permalink() . '"';
								}
							?> class="d-flex justify-content-start">
								<div class="class-num align-self-center px-2">
									<?php echo $i; ?>
								</div>
								<div class="class-title align-self-center">
									<?php the_title(); ?>
								</div>
								<div class="class-icon align-self-center ml-auto mr-4">
									<i class="fa fa-chevron-circle-right" aria-hidden="true"></i>
								</div>
							</a>
						</li>
						<?php endwhile; ?>
					</ul>
					<?php endif; wp_reset_query(); ?>
				</div>
			</div>
		</div>

	</article>

	<div class="entry-footer d-flex justify-content-end">
		<a href="<?php echo home_url('/home') ?>" class="mr-auto btn-black">返回主頁</a>
	</div>
</div>
<?php endwhile; ?>

<?php endif; ?>

</div>