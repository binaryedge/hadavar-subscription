
<footer class="content-info footer-top">
  <div class="container">
		<div class="row">
			<div class="col-6">
				<?php dynamic_sidebar('footer-left'); ?>
			</div>
			<div class="col-6">
				<?php dynamic_sidebar('footer-right'); ?>
			</div>
		</div>
  </div>
</footer>

<footer class="content-info footer-bottom">
  <div class="container">
		<!-- Public -->
		<div class="row justify-content-center">
			<div class="col-8">
				<?php dynamic_sidebar('footer'); ?>
			</div>
		</div>

		<div class="row">
			<div class="col">
				<div class="footer-copyright mt-4">
					<img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/footer-logo-2x.png" alt="<?php bloginfo('name'); ?>" width="113" class="mt-4">
					<p class="mt-2">&copy; <?php echo date('Y'); ?> Diakonia Logos Ltd.</p>
				</div>
			</div>
		</div>
  </div>
</footer>
