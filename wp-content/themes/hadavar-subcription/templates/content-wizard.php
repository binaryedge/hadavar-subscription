<?php

use Hadavar\Membership as HMember;

global $current_user;
get_currentuserinfo();

$user_credits = HMember\get_current_user_credits();

?>

<div class="row">
	<div class="col-sm-12">
		<div class="process-container px-4 py-5 mx-auto my-5">
			<h1><?php the_title(); ?></h1>
			<div id="get-started"></div>
		</div>
	</div>
</div>



<script>
	var book_credit = <?php echo $user_credits['book']; ?>,
		course_credit = <?php echo $user_credits['course']; ?>;
</script>
<script id="get-started-template" type="text/ractive">
<form action="<?php echo home_url('/') ?>" method="POST">
	<input type="hidden" name="action" value="update_coursesAndBooks">
	<input type="hidden" name="courses" value="{{courses}}">
	<input type="hidden" name="books" value="{{books}}">

	<ul class="process-bar">
		<li class-active="step == 0">Hello</li>
		<li class-active="step == 1">Support</li>
		<li class-active="step == 2">Select Books</li>
		<li class-active="step == 3">Select Courses</li>
		<li class-active="step == 4">Ready!</li>
	</ul>

	{{#if step == 0}}
	<div class="container">
		<div class="row">
			<div class="col-sm-2">
				<img class="pkg-icon mx-auto mb-4" src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/<?php echo HMember\get_current_user_subscription_name(); ?>.png" alt="<?php echo HMember\get_current_user_subscription_name(); ?>">
			</div>
			<div class="col-sm-10">
				<div class="intro mb-5">
					<p>Thank you for choosing our Partnership Program! You are currently subscribed to <strong><?php echo HMember\get_current_user_subscription_name(); ?></strong>, which allow you to select <strong><?php echo $user_credits['course']; ?> courses</strong> and <strong><?php echo $user_credits['book']; ?> books</strong></p>
					<p>You can select your desired books and courses in the following steps. Please note that you may only select once.</p>
					<p>If you have any questions, please contact us via 9857-8478.</p>
				</div>
			</div>
		</div>
		<div class="row justify-content-center">
			<div class="col-3">
				<a href="#" on-click="@this.next_step()" class="btn btn-green">Let's Begin!</a>
			</div>
		</div>
	</div>
	
	{{elseif step == 1}}
	<div class="container">
		<div class="row">
			<div class="col-sm-2">
				<img class="pkg-icon mx-auto mb-4" src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/<?php echo HMember\get_current_user_subscription_name(); ?>.png" alt="<?php echo HMember\get_current_user_subscription_name(); ?>">
			</div>
			<div class="col-sm-10">
				<div class="intro mb-5">
					<p>Your support is important to us. Please select one of our ministries that you would like to support with the subscription funds.</p>
					<p>
						<label>Select your ministry to support:</label>
						<select class="form-control" name="support">
							<option value="">Hadavar Biblical Museum</option>
						</select>
					</p>
				</div>
			</div>
		</div>
		<div class="row justify-content-center">
			<div class="col-3">
				<a href="#" on-click="@this.next_step()" class="btn btn-green">Continue</a>
			</div>
		</div>
	</div>

	{{elseif step == 2}}
	<div class="container">
		<div class="row">
			<div class="col-sm-2">
				<img class="pkg-icon mx-auto mb-4" src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/<?php echo HMember\get_current_user_subscription_name(); ?>.png" alt="<?php echo HMember\get_current_user_subscription_name(); ?>">
			</div>
			<div class="col-sm-10">
				<div class="intro mb-5">
					<p>You are currently subscribed to <strong><?php echo HMember\get_current_user_subscription_name(); ?></strong> plan.</p>
					<p>
						<img class="process-icon" src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/process-books.png" alt="book">
						You may now select <strong><?php echo $user_credits['book']; ?></strong> books.</p>
				</div>
			</div>
		</div>
		<div class="row">
		<?php
			$book_query = new WP_Query(array(
				'post_type' => 'books',
				'posts_per_page' => -1,
			));

			while ($book_query->have_posts()) : $book_query->the_post();
		?>
			<div class="col-4">
				<div class="card content-card mb-4" class-active="books.indexOf(<?php the_ID(); ?>) != -1" on-click="@this.toggle_book(<?php the_ID(); ?>, '<?php the_title() ?>')">
					<div class="card-header" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);">
				    <span class="invisible"><?php the_title(); ?></span>
				  </div>
					<div class="card-block">
						<h5 class="card-title text-center"><?php the_title(); ?></h5>
						<a href="<?= esc_url(home_url('/books-list/')); ?>#<?php echo get_the_ID(); ?>" class="btn-more" target="_blank"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></a>
					</div>
				</div>
			</div>
		<?php endwhile; wp_reset_query(); ?>
			
		</div>
		<div class="row justify-content-center">
			<div class="col-3">
				<a href="#" on-click="@this.prev_step()" class="btn btn-black">Back</a>
			</div>
			<div class="col-3">
				<a href="#" on-click="@this.next_step()" class="btn btn-green">Next</a>
			</div>
		</div>
	</div>
	
	{{elseif step == 3}}
	<div class="container">
		<div class="row">
			<div class="col-sm-2">
				<img class="pkg-icon mx-auto mb-4" src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/<?php echo HMember\get_current_user_subscription_name(); ?>.png" alt="<?php echo HMember\get_current_user_subscription_name(); ?>">
			</div>
			<div class="col-sm-10">
				<div class="intro mb-5">
					<p>You are currently subscribed to <strong><?php echo HMember\get_current_user_subscription_name(); ?></strong> plan.</p>
					<p>
						<img class="process-icon" src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/process-courses.png" alt="courses">
						You may now select <strong><?php echo $user_credits['course']; ?></strong> courses.</p>
				</div>
			</div>
		</div>
		<div class="row">
		<?php
			$course_query = new WP_Query(array(
				'post_type' => 'courses',
				'post_parent' => 0,
				'posts_per_page' => -1,
			));

			while ($course_query->have_posts()) : $course_query->the_post();
		?>
			<div class="col-3">
				<div class="card content-card courses-card mb-4" class-active="courses.indexOf(<?php the_ID(); ?>) != -1" on-click="@this.toggle_course(<?php the_ID(); ?>, '<?php the_title(); ?>')">
					<div class="card-header" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);">
				    <span class="invisible">Feature</span>
				  </div>
					<div class="card-block text-center">
						<h5 class="card-title"><?php the_title() ?></h5>
						<a href="<?= esc_url(home_url('/courses-list/')); ?>#<?php echo get_the_ID(); ?>" class="btn-more" target="_blank"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></a>
					</div>
				</div>
			</div>
		<?php endwhile; ?>
		</div>
		<div class="row justify-content-center">
			<div class="col-3">
				<a href="#" on-click="@this.prev_step()" class="btn btn-black">Back</a>
			</div>
			<div class="col-3">
				<a href="#" on-click="@this.next_step()" class="btn btn-green">Next</a>
			</div>
		</div>
	</div>

	{{elseif step == 4}}
	<div class="container">
		<div class="row">
			<div class="col-sm-2">
				<img class="pkg-icon mx-auto mb-4" src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/<?php echo HMember\get_current_user_subscription_name(); ?>.png" alt="<?php echo HMember\get_current_user_subscription_name(); ?>">
			</div>
			<div class="col-sm-10">
				<div class="intro mb-5">
					<p>The initiation process is completed! To summarize your selection:</p>
					<p>
						<img class="process-icon" src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/process-courses.png" alt="courses">
						You have selected <strong><?php echo $user_credits['course']; ?></strong> courses.</p>
					<ul class="final-step-list mb-4">
						{{#courses_title}}
						<li>{{.}}</li>
						{{/}}
					</ul>
					<p>
						<img class="process-icon" src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/process-books.png" alt="book">
						You have selected <strong><?php echo $user_credits['book']; ?></strong> books.</p>
					<ul class="final-step-list mb-4">
						{{#books_title}}
						<li>{{.}}</li>
						{{/}}
					</ul>
					<p>You are now ready to begin your journey as an official Hadavar Yeshiva partner! We hope you enjoy the learning experience with us!</p>
				</div>
			</div>
		</div>
		<div class="row justify-content-center">
			<div class="col-3">
				<a href="#" on-click="@this.prev_step()" class="btn btn-black">Back</a>
			</div>
			<div class="col-3">
				<input type="submit" class="btn btn-green" value="Let's Begin!" />
			</div>
		</div>
	</div>

	
	
	{{/if}}
</form>
</script>