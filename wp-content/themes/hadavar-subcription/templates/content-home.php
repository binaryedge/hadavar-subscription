<div class="hero-banner">
	<div class="container">
		<div class="row">
			<div class="col-sm-6 offset-sm-6 content">
				<h1><?php the_title(); ?></h1>
				<hr>
				<?php the_content(); ?>
			</div>
		</div>
	</div>
</div>


<div class="subscription-package">
	<div class="container">
		<div class="subscription-intro">
			<div class="row">	
				<div class="col-sm-5">
					<div class='embed-container'><?php the_field('intro_video'); ?></div>
				</div>
				<div class="col-sm-7">
					<h2><?php the_field('intro_title'); ?></h2>
					<hr>
					<?php the_field('intro_detail'); ?>
				</div>
			</div>
		</div>
		<div class="subscription-details">
			<div class="row">
				<div class="col-md-3">
					<div class="s-package">
		        <img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/hex-eternity-1.png" class="pkg-icon" alt="恒">
		        <ul class="s-list">
	            <li>每月定期個獻 <b>300</b> 港元  (或以上)<br>或<br>一次過奉獻 <b>3,600</b> 港元 (或以上)</li>
	            <li>自選書籍&nbsp;&nbsp; <b>一本</b></li>
	            <li>自選收看錄影課程&nbsp;&nbsp; <b>三個</b></li>
	          </ul>
		        <a href="<?= esc_url(home_url('/register')); ?>" class="btn btn-red">立刻登記</a>
			    </div>
				</div>
				<div class="col-md-3">
					<div class="s-package">
		        <img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/hex-word-1.png" class="pkg-icon" alt="道">
		        <ul class="s-list">
	            <li>每月定期個獻 <b>1,000</b> 港元  (或以上)<br>或<br>一次過奉獻 <b>12,000</b> 港元 (或以上)</li>
	            <li>自選書籍&nbsp;&nbsp; <b>兩本</b></li>
	            <li>自選收看錄影課程&nbsp;&nbsp; <b>六個</b></li>
	          </ul>
		        <a href="<?= esc_url(home_url('/register')); ?>" class="btn btn-purple">立刻登記</a>
			    </div>
				</div>
				<div class="col-md-3">
					<div class="s-package">
		        <img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/hex-preach-1.png" class="pkg-icon" alt="傳">
		        <ul class="s-list">
	            <li>每月定期個獻 <b>3,000</b> 港元  (或以上)<br>或<br>一次過奉獻 <b>36,000</b> 港元 (或以上)</li>
	            <li>自選書籍&nbsp;&nbsp; <b>三本</b></li>
	            <li>自選收看錄影課程&nbsp;&nbsp; <b>九個</b></li>
	          </ul>
		        <a href="<?= esc_url(home_url('/register')); ?>" class="btn btn-blue">立刻登記</a>
			    </div>
				</div>
				<div class="col-md-3">
					<div class="s-package">
		        <img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/hex-serve-1.png" class="pkg-icon" alt="職">
		        <ul class="s-list">
	            <li>每月定期個獻 <b>10,000</b> 港元  (或以上)<br>或<br>一次過奉獻 <b>120,000</b> 港元 (或以上)</li>
	            <li>自選書籍&nbsp;&nbsp; <b>四本</b></li>
	            <li>自選收看錄影課程&nbsp;&nbsp; <b>十二個</b></li>
	          </ul>
		        <a href="<?= esc_url(home_url('/register')); ?>" class="btn btn-black">立刻登記</a>
			    </div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="books-courses">
	<div class="container">
		<h2><?php the_field('books_courses_title'); ?></h2>
		<hr>
		<?php the_field('books_courses_detail'); ?>
		
		<div class="books-row">
			<div class="row">
				<div class="col-sm-3">
					<h3>
						<img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/home-books.png" class="bc-icons" alt="Books"> 書籍
					</h3>
					<p><?php the_field('books_info'); ?></p>
				</div>
				<div class="col-sm-9">
					<div class="books-carousel owl-carousel owl-theme">
					<?php
						$books_query = new WP_Query(array(
							'post_type' => 'books',
							'posts_per_page' => -1
						));
						while ($books_query->have_posts()): $books_query->the_post();
					?>
					  <div class="item d-flex align-items-center" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);">
					  	<a href="<?= esc_url(home_url('/books-list/')); ?>#<?php echo get_the_ID(); ?>" class="item-link">
				  			<h5><?php the_title(); ?></h5>
				  		</a>
					  </div>
					<?php endwhile; wp_reset_postdata(); ?>
					</div>

				</div>
			</div>
		</div>


		<div class="courses-row">
			<div class="row">
				<div class="col-sm-3">
					<h3>
						<img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/home-courses.png" class="bc-icons" alt="Courses"> 課程
					</h3>
					<p><?php the_field('courses_info'); ?></p>
				</div>
				<div class="col-sm-9">
					<div class="books-carousel owl-carousel owl-theme">

					<?php
						$courses_query = new WP_Query(array(
							'post_type' => 'courses',
							'posts_per_page' => -1, 
							'post_parent' => 0
						));
						while ($courses_query->have_posts()): $courses_query->the_post();
					?>

					  <div class="item d-flex align-items-center" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);">
					  	<a href="<?= esc_url(home_url('/courses-list')); ?>#<?php echo get_the_ID(); ?>" class="item-link">
				  			<h5><?php the_title(); ?></h5>
				  		</a>
					  </div>

					<?php endwhile; wp_reset_postdata(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="faq">
	<div class="container">
		<h2>常見問題</h2>
		<hr>
		
		<div id="accordion" class="faq-list" role="tablist" aria-multiselectable="true">
		<?php
			$faq_query = new WP_Query(array(
				'post_type' => 'faq',
				'posts_per_page' => -1,
				'order' => 'asc'
			));

			while ($faq_query->have_posts()): $faq_query->the_post();
		?>
			<div class="card">
				<div class="card-header" role="tab" id="faq-heading-<?php the_ID(); ?>">
					<h5 class="mb-0">
						<a data-toggle="collapse" data-parent="#accordion" href="#faq-collapse-<?php the_ID(); ?>" aria-expanded="true" aria-controls="#faq-collapse-<?php the_ID(); ?>" class="collapsed">
							<img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/accordion-arrow.png" class="accordion-arrow">
							<?php the_title(); ?>
						</a>
					</h5>
				</div>
				<div class="collapse" id="faq-collapse-<?php the_ID(); ?>" role="tabpanel" aria-labelledby="faq-heading-<?php the_ID(); ?>">
					<div class="card-block">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
		<?php endwhile; wp_reset_postdata(); ?>
		</div>
	</div>
</div>



<div class="partner">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h2><?php the_field('bop_title'); ?></h2>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-9">
				<p><?php the_field('bop_introduction'); ?></p>
			</div>
			<div class="col-sm-3">
				<a href="<?= esc_url(home_url('/register')); ?>" class="btn-red">立刻登記</a>
			</div>
		</div>
	</div>
</div>







