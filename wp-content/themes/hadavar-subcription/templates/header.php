<header class="banner">
	<div class="container-fluid">

		<!-- Logged In -->
		<?php if(is_user_logged_in()): ?>

			<?php if(is_page('register-success')): ?>

			<div class="row align-items-center">
				<div class="col">
					<a class="brand" href="<?= esc_url(home_url('/home')); ?>">
						<img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/logo-2x.png" alt="<?php bloginfo('name'); ?>" width="255">
					</a>
				</div>
			</div>

			<?php else: ?>

			<div class="row align-items-center">
				<div class="col">
					<ul class="nav dashboard-links">
						<li class="nav-item"><a href="<?= esc_url(home_url('/home')); ?>" class="nav-link">課程</a></li>
						<li class="nav-item"><a href="<?= esc_url(home_url('/home/books')); ?>" class="nav-link">書籍</a></li>
					</ul>
				</div>
				<div class="col">
					<a class="brand" href="<?= esc_url(home_url('/home')); ?>">
						<img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/logo-2x.png" alt="<?php bloginfo('name'); ?>" width="255">
					</a>
				</div>
				<div class="col">
					<div class="dropdown d-flex justify-content-end">
						<div class="dropdown-container">
						  <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						  	<?php echo get_avatar( get_current_user_id(), 24 ); ?> 
						    <?php $user_info = get_userdata(get_current_user_id());
						    	echo '<span>' . $user_info->first_name . "</span>\n";
						    ?>
						  </button>
						  <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
						  	<li>
						  		<a href="<?= esc_url(home_url('/home/profile')); ?>" class="dropdown-item">個人資料</a>
						  	</li>
						  	<li>
						  		<a href="<?= esc_url(home_url('/home/plan-modification/')); ?>" class="dropdown-item">申請更改計劃級別</a>
						  	</li>
						  	<li>
						  		<a href="<?= esc_url(home_url('/contact-us')); ?>" class="dropdown-item">聯絡我們</a>
						  	</li>
						  	<li>
						  		<a href="<?php echo wp_logout_url( home_url() ); ?>" class="dropdown-item">登出</a>
						  	</li>
						  </ul>
					  </div>
					</div>

				</div>
			</div>

			<?php endif; ?>
		
		<?php else: ?>

		<!-- Public -->
		<div class="row">
			<div class="col-sm-6">
				<a class="brand" href="<?= esc_url(home_url('/')); ?>">
					<img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/logo-2x.png" alt="<?php bloginfo('name'); ?>" width="255">
				</a>
			</div>
			<div class="col-sm-6">
				<nav class="nav-primary">
					<?php
					if (has_nav_menu('primary_navigation')) :
						wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']);
					endif;
					?>
				</nav>
			</div>
		</div>

		<?php endif; ?>
		
	</div>
</header>
