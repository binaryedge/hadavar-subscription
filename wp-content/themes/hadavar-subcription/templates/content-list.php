<div class="row">
	
	<h2 class="mt-4 mb-5">
		<?php if(is_page('books-list')): ?>
		<img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/home-books.png" class="bc-icons mr-3" alt="Books"> <?php the_title(); ?>
		<?php elseif(is_page('courses-list')): ?>
		<img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/home-courses.png" class="bc-icons mr-3" alt="Courses"> <?php the_title(); ?>
		<?php endif; ?>
	</h2>
</div>

<div class="row">
	<ul class="list-unstyled">

		<!-- Books List -->
		<?php if(is_page('books-list')): ?>
			<?php
				$books_query = new WP_Query(array(
					'post_type' => 'books',
					'posts_per_page' => -1
				));
				while ($books_query->have_posts()): $books_query->the_post();
			?>

		  <li class="media mb-5 p-4" id="<?php echo get_the_ID(); ?>">
		    <img class="d-flex mr-4" src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>">
		    <div class="media-body">
		      <h3 class="mt-0 mb-3"><?php the_title(); ?></h3>
		      <h5 class="mt-0 mb-2">Author: <?php the_field('author'); ?></h5>
		      <?php if (get_field('translator')): ?>
		      <h5 class="mt-0 mb-3">Translator: <?php the_field('translator'); ?></h5>
		    	<?php endif; ?>
		      <?php the_content(); ?>
		    </div>
		  </li>
		  <?php endwhile; wp_reset_postdata(); ?>

	  <!-- Courses List -->
	  <?php elseif(is_page('courses-list')): ?>
	  	<?php
	  		$courses_query = new WP_Query(array(
	  			'post_type' => 'courses',
	  			'posts_per_page' => -1,
	  			'post_parent' => 0
	  		));
	  		while ($courses_query->have_posts()): $courses_query->the_post();
	  	?>

	    <li class="media mb-5 p-4" id="<?php echo get_the_ID(); ?>">
	      <img class="d-flex mr-4" src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>">
	      <div class="media-body">
	        <h3 class="mt-0 mb-3"><?php the_title(); ?></h3>
	        <?php the_content(); ?>
	        <?php if (have_rows('additional_content')):
	        	while ( have_rows('additional_content') ) : the_row(); ?>

	        	<div class="card">
	        	  <div class="card-block">
	        	    <h5 class="card-title"><?php the_sub_field('title'); ?></h5>
	        	    <p class="card-text"><?php the_sub_field('content'); ?></p>
	        	  </div>
	        	</div>

	        <?php endwhile;
	        endif; ?>
	      </div>
	    </li>

	    <?php endwhile; wp_reset_postdata(); ?>

	  <?php endif; ?>

	</ul>
</div>





