<?php global $subscription_names; ?>

<?php if (isset($_GET['msg']) && $_GET['msg'] == '1'): ?>

<div class="row">
	<div class="col text-center">
		<h1>Thank you for choosing your subscription!</h1>
		<p>Our staff will contact you shortly.</p>
		<p><a href="<?= esc_url(home_url('/')); ?>" class="btn-blue mt-4">Return to Home</a></p>
	</div>
</div>

<?php else: ?>

<div class="row">
	<div class="col text-center">
		<?php the_content(); ?>
	</div>
</div>

<div class="row justify-content-between">
	<!-- <?php for ($i = 0; $i < 4; $i++) { ?>
	<div class="col-xs-3 text-xs-center">
		<form action="" method="POST">
			<img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/<?php echo $subscription_names[$i]; ?>.png" alt="<?php echo $subscription_names[$i]; ?>" class="pkg-icon">
			<input type="hidden" value="update_subscription" name="action">
			<input type="hidden" name="subscription" value="<?php echo $i; ?>">
			<input type="submit" value="choose" class="btn btn-block">
		</form>
	</div>
	<?php } ?>
</div> -->


	<div class="col-md-3">
		<div class="s-package">
			<form action="" method="POST">
	      <img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/hex-eternity-1.png" class="pkg-icon" alt="恒">
	      <ul class="s-list">
	        <li>每月定期個獻 <b>300</b> 港元  (或以上)<br>或<br>一次過奉獻 <b>3,600</b> 港元 (或以上)</li>
	        <li>自選書籍&nbsp;&nbsp; <b>一本</b></li>
	        <li>自選收看錄影課程&nbsp;&nbsp; <b>三個</b></li>
	      </ul>
	      <input type="hidden" value="update_subscription" name="action">
	      <input type="hidden" name="subscription" value="0">
	      <input type="submit" value="Enroll" class="btn btn-red">
	    </form>
    </div>
	</div>
	<div class="col-md-3">
		<div class="s-package">
			<form action="" method="POST">
	      <img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/hex-word-1.png" class="pkg-icon" alt="道">
	      <ul class="s-list">
	        <li>每月定期個獻 <b>1,000</b> 港元  (或以上)<br>或<br>一次過奉獻 <b>12,000</b> 港元 (或以上)</li>
	        <li>自選書籍&nbsp;&nbsp; <b>兩本</b></li>
	        <li>自選收看錄影課程&nbsp;&nbsp; <b>六個</b></li>
	      </ul>
	      <input type="hidden" value="update_subscription" name="action">
	      <input type="hidden" name="subscription" value="1">
	      <input type="submit" value="Enroll" class="btn btn-purple">
      </form>
    </div>
	</div>
	<div class="col-md-3">
		<div class="s-package">
			<form action="" method="POST">
	      <img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/hex-preach-1.png" class="pkg-icon" alt="傳">
	      <ul class="s-list">
	        <li>每月定期個獻 <b>3,000</b> 港元  (或以上)<br>或<br>一次過奉獻 <b>36,000</b> 港元 (或以上)</li>
	        <li>自選書籍&nbsp;&nbsp; <b>三本</b></li>
	        <li>自選收看錄影課程&nbsp;&nbsp; <b>九個</b></li>
	      </ul>
	      <input type="hidden" value="update_subscription" name="action">
	      <input type="hidden" name="subscription" value="2">
	      <input type="submit" value="Enroll" class="btn btn-blue">
      </form>
    </div>
	</div>
	<div class="col-md-3">
		<div class="s-package">
			<form action="" method="POST">
	      <img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/hex-serve-1.png" class="pkg-icon" alt="職">
	      <ul class="s-list">
	        <li>每月定期個獻 <b>10,000</b> 港元  (或以上)<br>或<br>一次過奉獻 <b>120,000</b> 港元 (或以上)</li>
	        <li>自選書籍&nbsp;&nbsp; <b>四本</b></li>
	        <li>自選收看錄影課程&nbsp;&nbsp; <b>十二個</b></li>
	      </ul>
	      <input type="hidden" value="update_subscription" name="action">
	      <input type="hidden" name="subscription" value="3">
	      <input type="submit" value="Enroll" class="btn btn-black">
      </form>
    </div>
	</div>

</div>


<?php endif; ?>