<?php

use Hadavar\Membership as HMember;


global $current_user;
get_currentuserinfo();

$user_credits = HMember\get_current_user_credits();

?>

<?php if ($_GET['msg'] == '1'): ?>
<div class="alert alert-success">你的課程和書籍已經更新！</div>
<?php elseif ($_GET['msg'] == '2'): ?>
	<div class="alert alert-warning">請聯絡夏達華工作人員 <a href="mailto:info@hadavar.org.hk">info@hadavar.org.hk</a></div>
<?php elseif ($_GET['msg'] == '3'): ?>
	<div class="alert alert-warning">我們正在驗證您的申請。我們將會在48小時之內聯絡你。</div>
<?php endif; ?>

<div class="dash-card mb-5">
	<div class="row">
		<div class="col-sm-6">
			<div class="d-inline-flex welcome">
				<i class="fa fa-user-circle-o mr-3" aria-hidden="true"></i> 歡迎, <?php echo $current_user->user_firstname; ?>!
			</div>
		</div>
		<div class="col-sm-6">
			<div class="d-inline-flex align-items-center p-2 pr-4">你現在正訂閱 <b>「<?php echo HMember\get_current_user_subscription_name(); ?>」</b> 計劃</div>
			<div class="d-inline-flex align-items-center p-2">
				<div class="align-self-center">
					<img src="<?php echo get_stylesheet_directory_uri() ?>/dist/	images/home-courses.png" class="mr-2" alt="Courses">
				</div>

				<div class="credit-count">
					<?php
						$course_permissions = get_field('course_permissions', "user_{$current_user->ID}");
						$with_course = is_array($course_permissions) ? count($course_permissions) : 0;
						echo $with_course;
					?> 聖經課程
				</div>
				
			</div>
			<div class="d-inline-flex align-items-center p-2">
				<div class="align-self-center">
					<img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/home-books.png" class="mr-2" alt="Books">
				</div>				
				<div class="credit-count">
					<?php
						$book_permissions = get_field('book_permissions', "user_{$current_user->ID}");
						$with_book = is_array($book_permissions) ? count($book_permissions) : 0;
						echo $with_book;
					?> 書籍
				</div>
			</div>
		</div>
	</div>
</div>

<?php

if ($with_course != 0) :

	$course_has_access = HMember\get_parent_coures_with_permissions();

	$course_query = new WP_Query(array(
		'post_type' => 'courses',
		'post__in' => $course_has_access,
		'posts_per_page' => -1,
		'post_parent' => 0,
	));
?>

<div class="row">
	<?php while ($course_query->have_posts()): $course_query->the_post();

		$course_status = HMember\get_course_status($post->ID);
	?>
		<div class="col-sm-4">
			<div class="card content-card courses-card mb-3">
				<div class="card-header" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);">
					<span class="invisible">Feature</span>
				</div>
				<div class="card-block text-center">
					<h4 class="card-title"><?php the_title(); ?></h4>
					<p class="card-text"><?php the_excerpt(); ?></p>
					<a href="<?php the_permalink(); ?>" class="btn btn-<?php echo $course_status == 1 ? 'green' : 'blue'; ?>">
						<?php if ($course_status == 0) {
							echo '修讀';
						} elseif ($course_status == 1) {
							echo '繼續';
						} else {
							echo '完成';
						} ?>
					</a>
				</div>
			</div>
		</div>
	<?php endwhile; wp_reset_query(); ?>
</div>

<?php else: ?>

<br><br><br><br><br>

<?php endif; ?>
