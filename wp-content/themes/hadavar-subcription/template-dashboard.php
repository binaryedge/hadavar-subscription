<?php
/**
 * Template Name: Dashboard Template
 */
?>

<div class="container">
	<?php while (have_posts()) : the_post(); ?>
		<?php get_template_part('templates/content', 'dashboard'); ?>
	<?php endwhile; ?>
</div>
