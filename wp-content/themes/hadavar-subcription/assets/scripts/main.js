/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

	// Use this variable to set up the common and page specific functions. If you
	// rename this variable, you will also need to rename the namespace below.
	var Sage = {
		// All pages
		'common': {
			init: function() {
				// Home page - carousels
				$(document).ready(function(){
					$('.books-carousel').owlCarousel({
						nav: true,
						margin: 10
					});
					$('.courses-carousel').owlCarousel({
						nav: true
					});
				});

				// Profile page - gender radiobox
				$('input:checked').parent('label').addClass('checked');
				$('input:radio').click(function() {
					$('input:radio').parent('label').removeClass('checked');
					$(this).parent('label').addClass('checked');
				});

				// Logged in dropdown
				$('.logged-in .dropdown-menu').css({
			    'width': ($('.logged-in .dropdown-container').width() + 'px')
			  });

			  $('.upme-errors').addClass('alert alert-danger');

			},
			finalize: function() {
			}
		},
		// Home page
		'home': {
			init: function() {
			},
			finalize: function() {
			}
		},
		'get_started': {
			init: function () {
				my_ractive = new Ractive({
					target: '#get-started',
					template: '#get-started-template',
					data: {
						step: 0,
						courses: [ ],
						courses_title: [ ],
						books: [ ],
						books_title: [ ],
						book_credit: book_credit,
						course_credit: course_credit
					},
					toggle_book: function (id, title) {
						var books = this.get('books');
						var c_remove_index = books.indexOf(id);

						if (c_remove_index < 0) {
							if (books.length >= this.get('book_credit')) {
								alert('You have selected more than ' + this.get('book_credit') + ' books available to you.');
								return false;
							} else {
								this.push('books', id);
								this.push('books_title', title);
							}
						} else {
							this.splice('books', c_remove_index, 1);
							this.splice('books_title', c_remove_index, 1);
						}

						return false;
					},
					toggle_course: function (id, title) {
						var courses = this.get('courses');
						var c_remove_index = courses.indexOf(id);

						if (c_remove_index < 0) {
							if (courses.length >= this.get('course_credit')) {
								alert('You have selected more than ' + this.get('course_credit') + ' courses available to you.');
								return false;
							} else {
								this.push('courses', id);
								this.push('courses_title', title);
							}
						} else {
							this.splice('courses', c_remove_index, 1);
							this.splice('courses_title', c_remove_index, 1);
						}

						return false;
					},

					next_step: function (ev) {
						var current_step = this.get('step');

						if (current_step === 2) {
							if (this.get('books').length !== this.get('book_credit')) {
								alert('Please select all ' + this.get('book_credit') + ' books available to you.');
								return false;
							}
						} else if (current_step === 3) {
							if (this.get('courses').length !== this.get('course_credit')) {
								alert('Please select all ' + this.get('course_credit') + ' courses available to you.');
								return false;
							}
						}

						this.set('step', this.get('step') + 1);
						return false;
					},
					prev_step: function (ev) {
						this.set('step', this.get('step') - 1);
						return false;
					},
					finish_step: function (ev) {
						return false;
					}
				});
			}
		},
		// About us page, note the change from about-us to about_us.
		'login': {
			init: function() {
				var loginForm = $('#upme-login-form-1').show(),
					forgotPForm = $('.upme-forgot-pass').hide();

				$('.upme-login-forgot-link, a[id^="upme-back-to-login"]').on('click', function (e) {
					loginForm.add(forgotPForm).toggle();
					e.preventDefault();
				});

				// add placeholder on forgot password form
				$("#user_name_email-1").attr("placeholder", "請輸入電郵");
			}
		},
		'register': {
			init: function () {
				var sp = $('.upme-separator:contains("個人資料")');

				sp.nextAll().wrapAll('<div class="r-step" style="display:none;" />');
				sp.prevAll().andSelf().wrapAll('<div class="r-step" />');
				sp.replaceWith('<button id="next-step" type="button">下一步</button>');

				$('#upme-register').before('<button id="prev-step" type="button">上一步</button>');

				$(document).on('click', '#next-step, #prev-step', function(e) {
					$('.r-step').toggle();

					event.preventDefault();
				});

				$('input:radio').click(function() {
					$('input:radio').parent('label').removeClass('checked');
					$(this).parent('label').addClass('checked');
				});

			}
		}
	};

	// The routing fires all common scripts, followed by the page specific scripts.
	// Add additional events for more control over timing e.g. a finalize event
	var UTIL = {
		fire: function(func, funcname, args) {
			var fire;
			var namespace = Sage;
			funcname = (funcname === undefined) ? 'init' : funcname;
			fire = func !== '';
			fire = fire && namespace[func];
			fire = fire && typeof namespace[func][funcname] === 'function';

			if (fire) {
				namespace[func][funcname](args);
			}
		},
		loadEvents: function() {
			// Fire common init JS
			UTIL.fire('common');

			// Fire page-specific init JS, and then finalize JS
			$.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
				UTIL.fire(classnm);
				UTIL.fire(classnm, 'finalize');
			});

			// Fire common finalize JS
			UTIL.fire('common', 'finalize');
		}
	};

	// Load Events
	$(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
