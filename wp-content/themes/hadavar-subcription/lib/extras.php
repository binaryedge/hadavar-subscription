<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Setup;
use Hadavar\Membership as HMember;

/**
 * Add <body> classes
 */
function body_class($classes) {
	// Add page slug if it doesn't exist
	if (is_single() || is_page() && !is_front_page()) {
		if (!in_array(basename(get_permalink()), $classes)) {
			$classes[] = basename(get_permalink());
		}
	}

	// Add class if sidebar is active
	if (Setup\display_sidebar()) {
		$classes[] = 'sidebar-primary';
	}

	return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');

/**
 * Clean up the_excerpt()
 */
function excerpt_more() {
	return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
}
add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');






// Shortcode actions

function request_plan_change () {
	if (isset($_POST['action']) && $_POST['action'] == 'request_plan_change') {
		global $current_user;
		get_currentuserinfo();

		$message = 'User Email: ' . $current_user->user_email . "\n";
		$message .= 'User ID: ' . $current_user->user_login . "\n";
		$message .= 'Request plan change to: ' . HMember\get_subscription_name($_POST['plan']) . "\n";

		wp_mail(get_option( 'admin_email' ), get_option( 'blogname' ) . ': Request Plan Change', $message);
	}
}

add_action('init', __NAMESPACE__ . '\\request_plan_change');


// Shortcodes

function request_change_form () {
	global $current_user;
	get_currentuserinfo();

	ob_start();
?>
<p class="mb-0">You are currently subscribed to plan - <strong><?php echo HMember\get_current_user_subscription_name(); ?></strong></p>
<p class="mb-0">Subscription started: <strong><?php the_field('subscription_start', "user_{$current_user->ID}"); ?></strong></p>
<p class="mb-4">Subscription end: <strong><?php the_field('subscription_end', "user_{$current_user->ID}"); ?></strong></p>

<form action="<?php echo home_url('/'); ?>" method="POST">
	<input type="hidden" name="request_plan_change">

	<p>Select your desired plan:</p>
	<p>
		<select name="plan" class="form-control">
			<option value="0">「恒」- 1 Book/3 Courses</option>
			<option value="1">「道」- 2 Books/6 Courses</option>
			<option value="2">「傳」- 3 Books/9 Courses</option>
			<option value="3">「職」- 4 Books/12 Courses</option>
		</select>
	</p>

	<p>
		<input type="submit" value="Submit Request" class="btn-green">
	</p>
</form>
<?php
	return ob_get_clean();
}
add_shortcode( 'request_change_form', __NAMESPACE__ . '\\request_change_form' );


function receipt_list () {
	global $current_user;

	ob_start();
?>
<div id="accordion" role="tablist" aria-multiselectable="true">
	<?php while (have_rows('receipts', "user_{$current_user->ID}")) : the_row(); ?>
	<div class="card">
		<div class="card-header" id="receipt-<?php echo get_row_index(); ?>">
			<h5 class="mb-0">
				<a data-toggle="collapse" data-parent="#accordion" href="#collapse-<?php echo get_row_index(); ?>" aria-expanded="true" aria-controls="collapse-<?php echo get_row_index(); ?>">
					<?php the_sub_field('date'); ?>
				</a>
			</h5>
		</div>

		<div id="collapse-<?php echo get_row_index(); ?>" class="collapse" role="tabpanel" aria-labelledby="receipt-<?php echo get_row_index(); ?>">
			<div class="card-block">
				<a href="<?php the_sub_field('file'); ?>" >Download File</a>
			</div>
		</div>
	</div>
	<?php endwhile; ?>
</div>
<?php
	return ob_get_clean();
}
add_shortcode( 'receipt_list', __NAMESPACE__ . '\\receipt_list' );




