<?php

namespace Hadavar\Membership;

require get_stylesheet_directory() . '/vendor/autoload.php';

use Carbon\Carbon;


$subscription_names = ['恒', '道', '傳', '職', ];
$subscription_course_credit_map = [ 3, 6, 9, 12 ];
$subscription_book_credit_map = [ 1, 2, 3, 4 ];
$wp_timezone = get_option('timezone_string');




// Getter

function get_timezone () {
	global $wp_timezone;
	return $wp_timezone;
}

function get_subscription_name ($sub_id) {
	global $subscription_names;
	return $subscription_names[$sub_id];
}

function get_current_user_subscription_name () {
	global $current_user;
	get_currentuserinfo();
	
	return get_subscription_name(get_field('subscription', "user_{$current_user->ID}"));
}

function get_current_user_credits () {
	global $current_user;
	get_currentuserinfo();

	$course_credit = get_field('course_credit', "user_{$current_user->ID}");
	$book_credit = get_field('book_credit', "user_{$current_user->ID}");

	return array(
		'course' => $course_credit ? $course_credit : 0,
		'book' => $book_credit ? $book_credit : 0,
	);
}








// API


function membership_action () {
	if (isset($_POST['action']) && !empty($_POST['action'])) {
		$action = $_POST['action'];

		if ($action == 'update_subscription') {
			update_subscription();
		} elseif ($action == 'update_coursesAndBooks') {
			update_coursesAndBooks();
		} elseif ($action == 'complete_course') {
			complete_course($_POST['course_ID']);
		} elseif ($action == 'undo_course') {
			undo_course($_POST['course_ID']);
		}
	} elseif (isset($_GET['action']) && !empty($_GET['action'])) {
		if ($_GET['action'] == 'reset_courseProgress' && current_user_can('edit_users')) {
			if (isset($_GET['parent_course_ID']) && !empty($_GET['parent_course_ID'])) {
				reset_courseProgress($_GET['parent_course_ID']);
			}
		}
	}
}

add_action('init', __NAMESPACE__ . '\\membership_action');



function update_subscription () {
	if (isset($_POST['subscription']) && is_numeric($_POST['subscription'])) {

		$my_sub = intval($_POST['subscription']);
		$my_id = 'user_' . get_current_user_id();

		update_field('subscription', $my_sub, $my_id);
		update_field('course_credit', 0, $my_id);
		update_field('book_credit', 0, $my_id);

		// ToDO: Send Email Notification to Admin
	}

	wp_redirect(home_url('/register/register-success/?msg=1'));
	exit();
}



function update_coursesAndBooks () {
	if (isset($_POST['courses']) && isset($_POST['books'])) {
		$courses = explode(',', $_POST['courses']);
		$books = explode(',', $_POST['books']);



		// Check to see if this request is valid/enough credits to apply for the provided courses
		$credits = get_current_user_credits();
		if (count($courses) > $credits['course'] || count($books) > $credits['book']) {
			wp_redirect(home_url('/home/?msg=2'));
			exit();
		}



		global $current_user;
		get_currentuserinfo();
		$my_id = "user_{$current_user->ID}";

		$carbon_now = Carbon::now(get_timezone());
		$carbon_then = Carbon::now(get_timezone())->addYear();

		$course_permissions = array();
		foreach ($courses as $cv) {
			$course_permissions[] = array(
				"process_date" => $carbon_now->format('d/m/Y'),
				"expiration_date" => $carbon_then->format('d/m/Y'),
				"course" => intval($cv),
			);
		}
		update_field('course_permissions', $course_permissions, $my_id);
		update_field('course_credit', 0, $my_id);

		$book_permissions = array();
		foreach ($books as $cv) {
			$book_permissions[] = array(
				"process_date" => $carbon_now->format('d/m/Y'),
				"book" => intval($cv),
			);
		}
		update_field('book_permissions', $book_permissions, $my_id);
		update_field('book_credit', 0, $my_id);

		update_field('support', $_POST['support'], $my_id);

		wp_redirect(home_url('/home/?msg=1'));
		exit();
	}

	wp_redirect(home_url('/home/'));
	exit();
}


// Course Progress API

/** In the following format:
	* array( course_ID => progress_status )
	*/

// 0/null - not started
// 1 - started
// 2 - finished
function get_course_progress ($parent_course_ID) {
	global $current_user;

	$course_childen = get_children(array(
		'post_parent' => $parent_course_ID,
	));

	$course_progress = get_user_meta($current_user->ID, "course_{$parent_course_ID}_progress", true);
	if (!$course_progress) {
		$course_progress = array();
		foreach ($course_childen as $c) {
			$course_progress[$c->ID] = 0;
		}
		update_user_meta($current_user->ID, "course_{$parent_course_ID}_progress", $course_progress);
	} else if (count($course_childen) != count($course_progress)) {
		foreach ($course_childen as $c) {
			if (!array_key_exists($c->ID, $course_progress)) {
				$course_progress[$c->ID] = 0;
			}
		}
		update_user_meta($current_user->ID, "course_{$parent_course_ID}_progress", $course_progress);
	}

	return $course_progress;
}

function set_course_progress ($course_ID, $parent_course_ID, $progress) {
	global $current_user;

	$course_progress = get_course_progress($parent_course_ID);
	$course_progress[$course_ID] = $progress;
	update_user_meta($current_user->ID, "course_{$parent_course_ID}_progress", $course_progress);
}

function reset_courseProgress ($parent_course_ID) {
	global $current_user;

	$course_childen = get_children(array(
		'post_parent' => $parent_course_ID
	));

	$course_progress = array();
	foreach ($course_childen as $c) {
		$course_progress[$c->ID] = 0;
	}
	update_user_meta($current_user->ID, "course_{$parent_course_ID}_progress", $course_progress);

	wp_redirect(get_permalink($parent_course_ID));
	exit();
}


function complete_course ($course_ID) {
	if ($course_ID) {
		$course_parent_ID = wp_get_post_parent_id($course_ID);
		set_course_progress($course_ID, $course_parent_ID, 2); // complete the current course
		
		if (isset($_POST['next_course_ID']) && !empty($_POST['next_course_ID'])) { // set next course to started
			set_course_progress($_POST['next_course_ID'], $course_parent_ID, 1);
		}

		wp_redirect(get_permalink($course_parent_ID));
		exit();
	}
}

function undo_course ($course_ID) {
	if ($course_ID) {
		$course_parent_ID = wp_get_post_parent_id($course_ID);
		set_course_progress($course_ID, $course_parent_ID, 1); // set the current course incompleted
		
		if (isset($_POST['next_course_ID']) && !empty($_POST['next_course_ID'])) { // set next course to started
			set_course_progress($_POST['next_course_ID'], $course_parent_ID, 0);
		}

		wp_redirect(get_permalink($course_parent_ID));
		exit();
	}
}


// 0 - Not started
// 1 - Started
// 2 - Finished
function get_course_status ($parent_course_ID) {
	if ($parent_course_ID) {
		$course_progress = get_course_progress($parent_course_ID);
		$coures_values = array_count_values($course_progress);

		if (
			($coures_values[0] == (count($course_progress) - 1) && $coures_values[1] == 1) ||
			$coures_values[0] == count($course_progress)
		) return 0;
		elseif ($coures_values[2] == count($course_progress)) return 2;
		else return 1;
	}
}














// Permission & Redirect

function protect_private_pages () {
	global $post;

	if (!is_user_logged_in()) {
		if (is_page('home') || $post->post_parent == 52) {
			wp_redirect(home_url('/register/login/'));
			exit();
		}
	} else {
		if (is_page('register') || $post->post_parent == 17) {
			// if ()
			if (is_page('register-success') && empty(get_current_user_subscription_name())) {

			} else {
				wp_redirect(home_url('/home/'));
			}
		}
	}
}
add_action('template_redirect', __NAMESPACE__ . '\\protect_private_pages');

function protect_register_success () {
	global $post;
	if (is_page('register-success') && isset($_GET['msg']) != '1') {
		$my_sub = get_field('subscription', "user_" . get_current_user_id());

		if ($my_sub != null) {
			wp_redirect(home_url('/'));
			exit();
		}
	}
}
add_action('template_redirect', __NAMESPACE__ . '\\protect_register_success');

function user_get_start () {
	global $post;
	if ((is_page('home') || $post->post_parent == 52) && !is_page('get-started') && $_GET['msg'] != '3') {
		$credits = get_current_user_credits();
		if ($credits['course'] > 0 || $credits['book'] > 0) {
			wp_redirect(home_url('/home/get-started/'));
			exit();
		} elseif ($credits['course'] == 0 && $credits['book'] == 0) {
			$my_id = 'user_' . get_current_user_id();
			$course_permissions = get_field('course_permissions', $my_id);

			if (!is_array($course_permissions) || count($course_permissions) == 0) {
				wp_redirect(home_url('/home/?msg=3'));
				exit();
			}
		}
	}
}
add_action('template_redirect', __NAMESPACE__ . '\\user_get_start');

function protect_course_pages () {
	global $post;
	if (is_singular('courses')) {
		$my_id = 'user_' . get_current_user_id();
		$course_permissions = get_field('course_permissions', $my_id);

		if (!$course_permissions) $course_permissions = array();
		
		if ($post->post_parent > 0) {
			$pass = false; // check if user have permission to pass the security check.
			foreach ($course_permissions as $cp) {
				$course_expiration = Carbon::createFromFormat('d/m/Y H:i', $cp['expiration_date'] . ' 23:59', get_timezone());
				$now = Carbon::now(get_timezone());

				if ($cp['course'] == $post->post_parent && $course_expiration->gte($now)) {
					$pass = true;
					break;
				}
			}

			if (!$pass) {
				$parent_url = get_permalink($post->post_parent);
				$parent_url  = add_query_string($parent_url, 'pv', 1);
				
				wp_redirect($parent_url);
				exit();
			}
		} else if ($post->post_parent == 0) {

			$have_acess = false;
			foreach ($course_permissions as $cp) {
				$course_expiration = Carbon::createFromFormat('d/m/Y H:i', $cp['expiration_date'] . ' 23:59', get_timezone());
				$now = Carbon::now(get_timezone());

				if ($cp['course'] == $post->ID && $course_expiration->gte($now)) {
					$have_acess = true;
					break;
				}
			}

			if (!$have_acess && !isset($_GET['pv'])) {
				// Preview post parent introduction
				// Redirect to preview Course parent page version
				$url = get_permalink($post);
				$url  = add_query_string($url, 'pv', 1);

				wp_redirect($url);
				exit();
			}
		}
	}
}
add_action('template_redirect', __NAMESPACE__ . '\\protect_course_pages');











// Helpers

function add_query_string ($url, $key, $value) {
	// $url   = 'http://example.com/?s=original&fq=category';
	$parts = parse_url($url);

	parse_str($parts['query'], $query);
	$query[$key] = $value;

	$new = sprintf(
		'%s://%s%s?%s'
		, $parts['scheme']
		, $parts['host']
		, $parts['path']
		, http_build_query($query)
	);

	return $new;
}

function get_parent_coures_with_permissions () {
	global $current_user;
	get_currentuserinfo();

	$now = Carbon::now(get_timezone());
	$course_permissions = get_field('course_permissions', 'user_' . $current_user->ID);
	$show_courses = array();

	foreach ($course_permissions as $cp) {
		$course_expiration = Carbon::createFromFormat('d/m/Y H:i', $cp['expiration_date'] . ' 23:59', get_timezone());

		if ($course_expiration->gte($now)) {
			$show_courses[] = $cp['course'];
		}
	}

	return $show_courses;
}

function get_books_with_permissions () {
	global $current_user;
	get_currentuserinfo();

	$now = Carbon::now(get_timezone());
	$book_permissions = get_field('book_permissions', 'user_' . $current_user->ID);
	$show_books = array();

	foreach ($book_permissions as $bp) {
		$show_books[] = $bp['book'];
	}

	return $show_books;
}










// History

// Datetime in d/m/Y g:i a format
function append_user_log ($user_ID, $details, $init_by = null) {
	$history = get_user_meta($user_ID, 'history', true);

	if (!$history || !is_array($history)) $history = array();

	$history[] = array(
		'date' => Carbon::now(get_timezone())->format('d/m/Y g:i a'),
		'details' => $details,
		'init_by_user_id' => $init_by,
	);

	update_user_meta( $user_ID, 'history', $history );
}





/**
 * Show history log button
 * 
 * @param  object $profileuser A WP_User object
 * @return void
 */
function show_user_history_log_btn ($profileuser) {
?>
	<h2 style="display: inline-block; margin-right: 5px;">Subscription Management</h2>
	<a style="top: auto;" href="http://localhost/hadavar-subscription/wp-admin/?user_history_log=<?php echo $profileuser->ID; ?>" class="page-title-action">Download User History Log</a>

	<style>
		#acf-form-data + h2 { display: none; }
	</style>
<?php
}
add_action('show_user_profile', __NAMESPACE__ . '\\show_user_history_log_btn', 9, 1);
add_action('edit_user_profile', __NAMESPACE__ . '\\show_user_history_log_btn', 9, 1);



function get_user_history_log () {
	if (is_admin() && current_user_can('edit_users') && isset($_GET['user_history_log'])) {
		$history = get_user_meta($_GET['user_history_log'], 'history', true);

		if (!$history || !is_array($history)) $history = array();

		$history_heading = [ 'Date', 'Details', 'Initiated By' ];
		if (count($history) > 0) $history_heading = array_keys($history[0]);


		$fp = fopen('php://output', 'w');
		fputcsv($fp, $history_heading);

		foreach ( $history as $line ) {
			fputcsv($fp, array_values($line));
		}
		fclose($fp);


		// header('Content-Type: application/excel');
		// header('Content-Disposition: attachment; filename="history.csv"');
		exit();
	}
}
add_action('init', __NAMESPACE__ . '\\get_user_history_log');





// User Log Action Hook

function sub_change_log ( $meta_id, $object_id, $meta_key, $_meta_value  ) {
	if ($meta_key == 'subscription') {
		append_user_log($object_id, 'Change of subscription to plan ' . get_subscription_name(intval($_meta_value)), get_current_user_id());
	} elseif ($meta_key == 'course_permissions') {
		$course_permissions = get_field('course_permissions', "user_{$object_id}");

		$details = '';
		foreach ($course_permissions as $cp) {
			$details .= "Course ID - {$cp['course']} were being added/updated, expiration date on {$cp['expiration_date']}\n";
		}
		append_user_log($object_id, $details, get_current_user_id());
	} elseif ($meta_key == 'book_permissions') {
		$book_permissions = get_field('book_permissions', "user_{$object_id}");

		$details = '';
		foreach ($book_permissions as $cp) {
			$details .= "Book ID - {$cp['book']} were being added\n";
		}
		append_user_log($object_id, $details, get_current_user_id());
	}
}
add_action('updated_user_meta', __NAMESPACE__ . '\\sub_change_log', 99, 4);



function user_change_log ($user_id) {
	append_user_log($user_id, 'User created.', get_current_user_id());
}
add_action( 'user_register', __NAMESPACE__ . '\\user_change_log', 10, 1 );















